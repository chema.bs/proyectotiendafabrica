CREATE TABLE Clientes (
  id INT NOT NULL AUTO_INCREMENT,
  Nombres VARCHAR(100) NULL,
  Apellidos VARCHAR(100) NULL,
  Direccion VARCHAR(100) NULL,
  Correo VARCHAR(45) NULL,
  Telefono VARCHAR(45) NULL,
  RFC VARCHAR(45) NULL,
  NumeroCB VARCHAR(45) NULL,
  PRIMARY KEY (id)
  );

CREATE TABLE Cotizaciones (
  id INT NOT NULL AUTO_INCREMENT,
  Modelo VARCHAR(45) NULL,
  Cantidad INT NULL,
  Precio DOUBLE NULL,
  PRIMARY KEY (id));

  CREATE TABLE ClientesCotizaciones (
    idCliente INT NOT NULL,
    idCotizacion INT NOT NULL,
    PRIMARY KEY (idCliente,idCotizacion)
  );

CREATE TABLE Proveedores (
  id INT NOT NULL AUTO_INCREMENT,
  Nombres VARCHAR(100) NULL,
  Apellidos VARCHAR(100) NULL,
  Direccion VARCHAR(100) NULL,
  Correo VARCHAR(45) NULL,
  Telefono VARCHAR(45) NULL,
  RFC VARCHAR(45) NULL,
  NumeroCB VARCHAR(45) NULL,
  PRIMARY KEY (id)
  );

CREATE TABLE Materiales (
  id INT NOT NULL AUTO_INCREMENT,
  Nombre VARCHAR(45) NULL,
  Ubicacion VARCHAR(45) NULL,
  Cantidad INT DEFAULT 0,
  Costo DOUBLE NULL,
  PRIMARY KEY (id)
  );

  CREATE TABLE ProveedoresMateriales (
    idProveedor INT NOT NULL,
    idMaterial INT NOT NULL,
    PRIMARY KEY (idProveedor,idMaterial)
  );

CREATE TABLE Productos (
  id INT NOT NULL AUTO_INCREMENT,
  Modelo VARCHAR(45) NULL,
  Precio VARCHAR(45) NULL,
  Cantidad INT DEFAULT 0,
  Ubicacion VARCHAR(45) DEFAULT '',
  PRIMARY KEY (id)
);

CREATE TABLE ProductosMateriales (
  idProducto INT NOT NULL,
  idMaterial INT NOT NULL,
  PRIMARY KEY (idProducto,idMaterial)
);

CREATE TABLE Ventas (
  idVenta INT NOT NULL AUTO_INCREMENT,
  idProducto INT NOT NULL,
  PRIMARY KEY (idVenta,idProducto)
);
