package datos;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validaciones {
        public static String[] tryPhone(String lexema){
        String[] out={"",""};
        Pattern pat = Pattern.compile("^[[0-9]+[\\s]+[0-9]+]+$");//patron {1}indica las veces
        Matcher mat = pat.matcher(lexema);
        if (mat.matches()==true){
            out[0]="true";
            out[1]=lexema;
        }else if (mat.matches()==false){
            out[0]="false";
            out[1]="";
        }
        return out;
    }

    public static String[] tryRFC(String lexema){
        String[] out={"",""};
        Pattern pat1 = Pattern.compile("^[A-Z|0-9]{10,13}");
        Matcher mat1 = pat1.matcher(lexema);
        if (mat1.matches()==true){
            out[0]="true";
            out[1]=lexema;
        }else if(mat1.matches()==false){
            out[0]="true";
            out[1]="";
        }
        return out;
    }
    public static String[] tryMoney(String lexema){
        String[] out = {"",""};
        Pattern pat1 = Pattern.compile("^[0-9]+[\\.][0-9]+?");//patron {1}indica las veces
        Pattern pat2 = Pattern.compile("[0-9]+");
        Matcher mat1 = pat1.matcher(lexema);//emparejamiento
        Matcher mat2 = pat2.matcher(lexema);
        if (mat1.matches()==true || mat2.matches()==true){
            out[0]="true";
            out[1]=lexema;
        }else{
            out[0]="false";
            out[1]="";
        }
        return out;
    }
    public static String[] tryInt(String lexema) {
        String[] out = {"",""};
        Pattern pat = Pattern.compile("[0-9]+");
        Matcher mat = pat.matcher(lexema);
        if (mat.matches()==true) {
            out[0]="true";
            out[1]=lexema;
        }else{
            out[0]="false";
            out[1]="";
        }
        return out;
    }
    public static double perserDouble(String in){
    double out;
    try {
        out=Double.parseDouble(in);
    }catch (NumberFormatException e){
        out=0;
    }
    return out;
    }
    public static int perserInteger(String in){
    int out;
    try {
        out=Integer.parseInt(in);
    }catch (NumberFormatException e){
        out=0;
    }
    return out;
    }
}
