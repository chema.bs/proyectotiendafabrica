package datos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import objetos.Persona;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetos.Material;
import objetos.Producto;
import objetos.Cotizacion;

/**
 *
 * @author xam-lx
 */
public class Modificaciones {
    String[] conexion = {"192.168.124.230","TiendaFinal","xam-db","itvirtual"};
    public void modificacionDePersonas(ArrayList<Persona> personasMdf, ArrayList<Persona> personasOld, String tipo){
        Connection con;
        try {
            con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
        for (int i=0; i<personasMdf.size(); i++) {
            String updateQuery = "UPDATE "+tipo+
                    " SET Nombres = ?,"+
                        " Apellidos = ?,"+
                        " Direccion = ?,"+
                        " Correo = ?,"+
                        " Telefono = ?,"+
                        " RFC = ?,"+
                        " NumeroCB = ?"+
                    " WHERE (Nombres = ?"+
                        " AND Apellidos = ?"+
                        " AND Direccion = ?"+
                        " AND Correo = ?"+
                        " AND Telefono = ?"+
                        " AND RFC = ?"+
                        " AND NumeroCB = ?);";
            PreparedStatement preparedStatement = con.prepareStatement(updateQuery);// Sentencia SET
            preparedStatement.setString(1,personasMdf.get(i).getNombres());
            preparedStatement.setString(2,personasMdf.get(i).getApellidos());
            preparedStatement.setString(3,personasMdf.get(i).getDireccion());
            preparedStatement.setString(4,personasMdf.get(i).getCorreo());
            preparedStatement.setString(5,personasMdf.get(i).getTelefono());
            preparedStatement.setString(6,personasMdf.get(i).getRFC());
            preparedStatement.setString(7,personasMdf.get(i).getNumeroCB());// Sentencia WHERE
            preparedStatement.setString(8,personasOld.get(i).getNombres());
            preparedStatement.setString(9,personasOld.get(i).getApellidos());
            preparedStatement.setString(10,personasOld.get(i).getDireccion());
            preparedStatement.setString(11,personasOld.get(i).getCorreo());
            preparedStatement.setString(12,personasOld.get(i).getTelefono());
            preparedStatement.setString(13,personasOld.get(i).getRFC());
            preparedStatement.setString(14,personasOld.get(i).getNumeroCB());
            preparedStatement.execute();
        }
        con.close();
        JOptionPane.showMessageDialog(null, "Actualización correcta!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con la base de datos!");
        }
    }
    public void modificacionDeProductos(ArrayList<Producto> productosOld, ArrayList<Producto> productosMdf){
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            for (int i=0; i<productosMdf.size(); i++) {
                String udateQuery = "UPDATE Productos"+
                        " SET Modelo = ?,"+
                        " Precio = ?,"+
                        " Cantidad = ?"+
                        " WHERE (Modelo = ?"+
                        " AND Precio = ?"+
                        " AND Cantidad = ?);";
                PreparedStatement preparedStatement = con.prepareStatement(udateQuery);
                preparedStatement.setString(1, productosMdf.get(i).getModelo());
                preparedStatement.setDouble(2, productosMdf.get(i).getPrecio());
                preparedStatement.setInt(3, productosMdf.get(i).getCantidad());
                preparedStatement.setString(4, productosOld.get(i).getModelo());
                preparedStatement.setDouble(5, productosOld.get(i).getPrecio());
                preparedStatement.setInt(6, productosOld.get(i).getCantidad());
                preparedStatement.execute();
            }
        con.close();
        JOptionPane.showMessageDialog(null, "Actualización correcta!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con la base de datos!");
        }
    }
    public void modificacionDeMateriales(ArrayList<Material> materialesOld, ArrayList<Material> materialesMdf){
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            for (int i=0; i<materialesMdf.size(); i++){
                String updateQuery = "UPDATE Materiales"+
                        " SET Nombre = ?,"+
                        " Cantidad = ?,"+
                        " Costo = ?"+
                        " WHERE(Nombre = ?"+
                        " AND Cantidad = ?"+
                        " AND Costo = ?);";
                PreparedStatement preparedStatement = con.prepareStatement(updateQuery);
                preparedStatement.setString(1, materialesMdf.get(i).getNombre());
                preparedStatement.setInt(2, materialesMdf.get(i).getCantidad());
                preparedStatement.setDouble(3, materialesMdf.get(i).getCosto());
                preparedStatement.setString(4, materialesOld.get(i).getNombre());
                preparedStatement.setInt(5, materialesOld.get(i).getCantidad());
                preparedStatement.setDouble(6, materialesOld.get(i).getCosto());
                preparedStatement.execute();
            }
            con.close();
            JOptionPane.showMessageDialog(null, "Actualización correcta!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con la base de datos!");
        }
    }
    public void modificacionDeCotizaciones(ArrayList<Cotizacion> cotizacionesMdf, ArrayList<Cotizacion> cotizacionesOld){
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            for (int i=0; i<cotizacionesMdf.size(); i++) {
                String updateQuery = "UPDATE Cotizaciones"+
                        " SET Modelo = ?,"+
                        " Cantidad = ?,"+
                        " Precio = ?"+
                        " WHERE(Modelo = ?"+
                        " AND Cantidad = ?"+
                        " AND Precio = ?);";
                PreparedStatement preparedStatement = con.prepareStatement(updateQuery);
                preparedStatement.setString(1, cotizacionesMdf.get(i).getModelo());
                preparedStatement.setInt(2, cotizacionesMdf.get(i).getCantidad());
                preparedStatement.setDouble(3, cotizacionesMdf.get(i).getPrecio());
                preparedStatement.setString(4, cotizacionesOld.get(i).getModelo());
                preparedStatement.setInt(5, cotizacionesOld.get(i).getCantidad());
                preparedStatement.setDouble(6, cotizacionesOld.get(i).getPrecio());
                preparedStatement.execute();
            }
            con.close();
            JOptionPane.showMessageDialog(null, "Actualización correcta!");
        } catch (SQLException ex) {
            Logger.getLogger(Modificaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
