/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetos.Cotizacion;
import objetos.Material;
import objetos.Persona;
import objetos.Producto;


/**
 *
 * @author xam-lx
 */
public class Bajas {
    //String[] conexion = {"192.168.124.230","TiendaFinal","xam-db","itvirtual"};
    String[] conexion = {"127.0.0.1","tienda","root","mn77tFyrfpMJVrqHoY"};
    public void bajaDePersonas(ArrayList<Persona> personasBj, String tipo){
        String query = "";
        String tablaEli = tipo+"_Eliminados";
        switch(tipo){
            case "Clientes": query = "{ call spr_CliDelete(?) }";
            break;
            case "Proveedores": query="{call spr_ProvDelete(?)}";
            break;
            default:
            break;    
        }
        
        try (Connection con = DriverManager.getConnection("jdbc:mariadb://"+conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);){
            
            //CallableStatement stmt = con.prepareCall(query);
            int ax = JOptionPane.showConfirmDialog(null, "¿Seguro que deseas borrar todos los datos en la lista?");
            if (ax==JOptionPane.YES_OPTION){
                for (int i=0; i<personasBj.size(); i++) {
                    //stmt.setInt(1, candidateId);
                    //Statement statment = con.createStatement();
                    /*/String deleteQuery = "DELETE FROM "+tipo+
                            " WHERE Nombres = '"+personasBj.get(i).getNombres()+"'"+
                        " AND Apellidos = '"+personasBj.get(i).getApellidos()+"'"+
                        " AND Direccion = '"+personasBj.get(i).getDireccion()+"'"+
                        " AND Correo = '"+personasBj.get(i).getCorreo()+"'"+
                        " AND Telefono = '"+personasBj.get(i).getTelefono()+"'"+
                        " AND RFC = '"+personasBj.get(i).getRFC()+"'"+
                        " AND NumeroCB = '"+personasBj.get(i).getNumeroCB()+"' LIMIT 1;";
                    /*/
                    //statment.executeQuery(deleteQuery);
                    CallableStatement stmt = con.prepareCall(query);
                        String rfc = personasBj.get(i).getRFC();
                        
                        stmt.setString(1, rfc);
                        //stmt.setString(2, tipo);
                        //stmt.setString(3, tablaEli);

                        stmt.execute();
                }
                con.close();
                if(personasBj.size()>1){
                    JOptionPane.showMessageDialog(null, "Se eliminaron todos los elementos de la lista");
                }else{
                    JOptionPane.showMessageDialog(null, "Se elimino un elemento de la lista");
               }
                
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con la base de datos!");
        }
    }
    
    public void bajaDeProductos(ArrayList<Producto> productosRm){
        String query = "{ call spr_ProdDelete(?) }";
        
        try (Connection con = DriverManager.getConnection("jdbc:mariadb://"+conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);){
            
            int ax = JOptionPane.showConfirmDialog(null, "¿Seguro que deseas borrar todos los datos en la lista?");
            if (ax==JOptionPane.YES_OPTION){
                
                for (int i=0; i<productosRm.size(); i++) {
                    CallableStatement stmt = con.prepareCall(query);
                        String modelo = productosRm.get(i).getModelo();
                        
                        stmt.setString(1, modelo);

                        stmt.execute();
                    
                    /*/Statement statement = con.createStatement();
                    String deleteQuery = "DELETE FROM Productos"+
                            " WHERE Modelo = '"+productosRm.get(i).getModelo()+"'"+
                            " AND Precio = "+productosRm.get(i).getPrecio()+""+
                            " AND Cantidad = "+productosRm.get(i).getCantidad()+" LIMIT 1;";
                    statement.executeQuery(deleteQuery);/*/
                }
                con.close();
                if(productosRm.size()>1){
                    JOptionPane.showMessageDialog(null, "Se eliminaron todos los elementos de la lista");
                }else{
                    JOptionPane.showMessageDialog(null, "Se elimino un elemento de la lista");
                }
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con base de datos contacte a soporte!");
        }
    }
    public void bajaDeMateriales(ArrayList<Material> materialesBj){
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            int ax = JOptionPane.showConfirmDialog(null, "¿Seguro que deseas borrar todos los datos en la lista?");
            if (ax==JOptionPane.YES_OPTION){
                for (int i=0; i<materialesBj.size(); i++){
                    Statement statement = con.createStatement();
                    String deleteQuery = "DELETE FROM Materiales"+
                            " WHERE Nombre = '"+materialesBj.get(i).getNombre()+"'"+
                            " AND Cantidad = "+materialesBj.get(i).getCantidad()+
                            " AND Costo = "+materialesBj.get(i).getCosto()+" LIMIT 1;";
                    statement.executeQuery(deleteQuery);                            
                }
                con.close();
                JOptionPane.showMessageDialog(null, "Borrados Correctamente!");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con base de datos contacte a soporte!");
        }
    }
    public void bajaDeCotizaciones(ArrayList<Cotizacion> cotizacionesBj){
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            int ax = JOptionPane.showConfirmDialog(null, "¿Seguro que deseas borrar todos los datos en la lista?");
            if (ax==JOptionPane.YES_OPTION){
                for (int i=0; i<cotizacionesBj.size(); i++) {
                    Statement statement = con.createStatement();
                    String deleteQuery = "DELETE FROM Cotizaciones"+
                            " WHERE Modelo = '"+cotizacionesBj.get(i).getModelo()+"'"+
                            " AND Cantidad = "+cotizacionesBj.get(i).getCantidad()+
                            " AND Precio = "+cotizacionesBj.get(i).getPrecio()+" LIMIT 1;";
                    statement.executeQuery(deleteQuery);
                }
                con.close();
                JOptionPane.showMessageDialog(null, "Borrados Correctamente!");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con base de datos contacte a soporte!");
        }
    }
    
}
