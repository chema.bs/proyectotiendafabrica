/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetos.Cotizacion;
import objetos.Material;
import objetos.Persona;
import objetos.Producto;

/**
 *
 * @author xam-lx
 */
public class Consultas {
    String[] conexion = {"127.0.0.1","tienda","root","mn77tFyrfpMJVrqHoY"};
    //String[] conexion = {"192.168.124.230","TiendaFinal","xam-db","itvirtual"};
    public ArrayList<Persona> busquedaDePersonas(Persona persona, String tipo){
        ArrayList<Persona> outPersonas = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String query = "SELECT * FROM "+tipo+
                    " WHERE Nombres LIKE '%"+persona.getNombres()+"%' " +
                    "AND Apellidos LIKE '%"+persona.getApellidos()+"%' " +
                    "AND Direccion LIKE '%"+persona.getDireccion()+"%' " +
                    "AND Correo LIKE '%"+persona.getCorreo()+"%' " +
                    "AND Telefono LIKE '%"+persona.getTelefono()+"%' " +
                    "AND RFC LIKE '%"+persona.getRFC()+"%' " +
                    "AND NumeroCB LIKE '%"+persona.getNumeroCB()+"%';";
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            con.close();
            while(resultSet.next()==true){
                outPersonas.add(new Persona(
                        resultSet.getString("Nombres"),
                        resultSet.getString("Apellidos"),
                        resultSet.getString("Direccion"),
                        resultSet.getString("Correo"),
                        resultSet.getString("Telefono"),
                        resultSet.getString("RFC"),
                        resultSet.getString("NumeroCB"))
                );
            }
        } catch (SQLException ex) {
            //ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error de conexion con base de datos!");
        }

        return outPersonas;
    }
    public ArrayList<Producto> busquedaDeProductos(Producto producto){
        ArrayList<Producto> outProductos = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String query = "SELECT * FROM Productos "+
                    "WHERE Modelo LIKE '%"+producto.getModelo()+"%';";
                    /*"AND Precio LIKE '%"+producto.getPrecio()+"%' "+*/
                    /*"AND Cantidad LIKE '%"+producto.getCantidad()+"%';";*/
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            con.close();
            while (resultSet.next()==true){
                outProductos.add(new Producto(
                resultSet.getString("Modelo"),
                Double.parseDouble(resultSet.getString("Precio")),
                resultSet.getInt("Cantidad")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion con base de datos!");
        } 
        return outProductos;
    }
    public ArrayList<Material> busquedaDemateriales(Material materialSch){
        ArrayList<Material> materiales = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String querySch = "SELECT * FROM Materiales "+
                    "WHERE Nombre LIKE '%"+materialSch.getNombre()+"%';";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(querySch);
            while (resultSet.next()==true){
            materiales.add(new Material(
                    resultSet.getString("Nombre"), 
                    resultSet.getInt("Cantidad"), 
                    resultSet.getDouble("Costo")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return materiales;
    }
    public ArrayList<Cotizacion> busquedaDeCotizaciones(Cotizacion cotizacion){
        ArrayList<Cotizacion> cotizaciones = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String querySch = "SELECT * FROM Cotizaciones "+
                    "WHERE Modelo LIKE '%"+cotizacion.getModelo()+"%';";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(querySch);
            while (resultSet.next()==true) {                
                cotizaciones.add(new Cotizacion(
                        resultSet.getString("Modelo"), 
                        resultSet.getInt("Cantidad"), 
                        resultSet.getDouble("Precio")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cotizaciones;
    }
}
