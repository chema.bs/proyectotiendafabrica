/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import objetos.Persona;
import objetos.Material;
import objetos.Producto;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetos.Cotizacion;

/**
 *
 * @author xam-lx
 */
public class Altas {
    ArrayList<Persona> clientes = new ArrayList<>();
    ArrayList<Persona> proveedores = new ArrayList<>();
    ArrayList<Material> materiales = new ArrayList<>();
    ArrayList<Producto> productos = new ArrayList<>();
    
    //String[] conexion = {"192.168.124.230","TiendaFinal","xam-db","itvirtual"};
    String[] conexion = {"127.0.0.1","tienda","root","mn77tFyrfpMJVrqHoY"};
    public void altaDePersonas(Persona persona, String tipo){
        try {          
            Connection connection = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String insertQuery = "INSERT INTO "+tipo+"(Nombres,Apellidos,Direccion,Correo,Telefono,RFC,NumeroCB)VALUES(?,?,?,?,?,?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1,persona.getNombres());
            preparedStatement.setString(2,persona.getApellidos());
            preparedStatement.setString(3,persona.getDireccion());
            preparedStatement.setString(4,persona.getCorreo());
            preparedStatement.setString(5,persona.getTelefono());
            preparedStatement.setString(6,persona.getRFC());
            preparedStatement.setString(7,persona.getNumeroCB());
            preparedStatement.execute();
            connection.close();
            //System.out.println(insertQuery);
            if (tipo=="Clientes") {
                JOptionPane.showMessageDialog(null,"El Cliente: "+persona.getNombres()+" fue dado de alta!");    
            }
            if (tipo=="Proveedores") {
                JOptionPane.showMessageDialog(null,"El Proveedor: "+persona.getNombres()+" fue dado de alta!");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Los datos de conexión estan mal, contacte a soporte!");
        }
    }
    public void altaDeProductos(Producto producto){
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String insertQuery = "INSERT INTO Productos(Modelo,Precio,Cantidad,Ubicacion)VALUES(?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1,producto.getModelo());
            preparedStatement.setDouble(2,producto.getPrecio());
            preparedStatement.setInt(3,producto.getCantidad());
            preparedStatement.setString(4, "");
            preparedStatement.execute();
            connection.close();
            JOptionPane.showMessageDialog(null, "El Producto: "+producto.getModelo()+" fue dado de alta!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Los datos de conexión estan mal, contacte a soporte!");           
        }
    }
    
    public void altaDeMateriales(Material material){
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String query = "INSERT INTO Materiales(Nombre,Cantidad,Costo)VALUES(?,?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, material.getNombre());
            preparedStatement.setInt(2, material.getCantidad());
            preparedStatement.setDouble(3, material.getCosto());
            preparedStatement.execute();
            connection.close();
            JOptionPane.showMessageDialog(null, "El material: "+material.getNombre()+" fue dado de alta!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexión con la base de datos contacte a soporte!");
        }
    }
    public void altaDeCotizaciones(Cotizacion cotizacion){
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String query = "INSERT INTO Cotizaciones(Modelo,Cantidad,Precio)VALUES(?,?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, cotizacion.getModelo());
            preparedStatement.setInt(2, cotizacion.getCantidad());
            preparedStatement.setDouble(3, cotizacion.getPrecio());
            preparedStatement.execute();
            connection.close();
            JOptionPane.showMessageDialog(null, "El material: "+cotizacion.getModelo()+" fue dado de alta!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexión con la base de datos contacte a soporte!");
        }
    }
    public void relacionClientes(int idCliente, int idCotizacion){
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String query = "INSERT INTO ClientesCotizaciones(idCotizacion,idCliente)VALUES("+
                    idCotizacion+","+idCliente+");";
            Statement statement = connection.createStatement();
            statement.executeQuery(query);
        } catch (SQLIntegrityConstraintViolationException e){
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error de conexion con la base de datos!");
        }
    }
    
}
