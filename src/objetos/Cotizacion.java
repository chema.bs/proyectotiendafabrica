package objetos;

public class Cotizacion {
    private String Modelo;
    private int Cantidad;
    private double Precio;
    public Cotizacion(String Modelo, int Cantidad, double Precio){
        this.Modelo=Modelo;
        this.Cantidad=Cantidad;
        this.Precio=Precio;
    }  
    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }  
}
