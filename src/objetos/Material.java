
package objetos;

public class Material {
    private String Nombre; 
    private int Cantidad;
    private double Costo;
    
    public Material (
        String Nombre, int Cantidad, double Costo){
        this.Nombre=Nombre;
        this.Cantidad=Cantidad;
        this.Costo=Costo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public double getCosto() {
        return Costo;
    }

    public void setCosto(double Costo) {
        this.Costo = Costo;
    }
}
