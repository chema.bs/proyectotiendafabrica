/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author xam-lx
 */
public class Persona {
    private String Nombres;
    private String Apellidos;
    private String Direccion;
    private String Correo;
    private String Telefono;
    private String RFC;
    private String NumeroCB;
    
    public Persona(
    String Nombres, String Apellidos, String Direccion, String Correo, String Telefono, String RFC, String NumerCB){
        this.Nombres=Nombres;
        this.Apellidos=Apellidos;
        this.Direccion=Direccion;
        this.Correo=Correo;
        this.Telefono=Telefono;
        this.RFC=RFC;
        this.NumeroCB=NumerCB;
    }
    
    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public String getNumeroCB() {
        return NumeroCB;
    }

    public void setNumeroCB(String NumeroCB) {
        this.NumeroCB = NumeroCB;
    }
    
    
}
