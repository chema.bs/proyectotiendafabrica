
package objetos;

public class Producto {
    private String Modelo;
    private int Cantidad;
    private double Precio;
    
    public Producto(
    String Modelo, double Precio, int Cantidad){
        this.Modelo=Modelo;
        this.Cantidad=Cantidad;
        this.Precio=Precio;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Costo) {
        this.Precio = Costo;
    }
}
