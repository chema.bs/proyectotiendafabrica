
package flujos;

import datos.Altas;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetos.Cotizacion;
import objetos.Persona;

public class Ventas {
    String[] conexion = {"192.168.124.230","TiendaFinal","xam-db","itvirtual"};
    public void vender(ArrayList<Cotizacion> cotizaciones, ArrayList<Persona> clientes){
            try {
                Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                        conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
                int idCliente=0,idCotizacion=0;
                if (cotizaciones.size() > clientes.size()){
                    for (int i=0; i<cotizaciones.size(); i++) {
                        for (int j=0; j<clientes.size(); j++) {
                            String queryCliente = "SELECT id FROM Clientes WHERE("+
                                    " Nombres = '"+clientes.get(j).getNombres()+"'"+
                                    " AND Apellidos = '"+clientes.get(j).getApellidos()+"'"+
                                    " AND Direccion = '"+clientes.get(j).getDireccion()+"'"+
                                    " AND Correo = '"+clientes.get(j).getCorreo()+"'"+
                                    " AND Telefono = '"+clientes.get(j).getTelefono()+"'"+
                                    " AND RFC = '"+clientes.get(j).getRFC()+"'"+
                                    " AND NumeroCB = '"+clientes.get(j).getNumeroCB()+"');";
                            Statement statement1 = con.createStatement();
                            ResultSet resultSet1 = statement1.executeQuery(queryCliente);
                            if (resultSet1.next()==false){}
                            else {
                                resultSet1.first();
                                idCliente = resultSet1.getInt("id");
                            }
                            String queryCotizacion = "SELECT id From Cotizaciones WHERE("+
                                    " Modelo = '"+cotizaciones.get(i).getModelo()+"'"+
                                    " AND Cantidad = "+cotizaciones.get(i).getCantidad()+
                                    " AND Precio = "+cotizaciones.get(i).getPrecio()+");";
                            Statement statement2 = con.createStatement();
                            ResultSet resultSet2 = statement2.executeQuery(queryCotizacion);
                            if (resultSet2.next()==false){}
                            else{
                                resultSet2.first();
                                idCotizacion = resultSet2.getInt("id");
                            }
                            Altas altas = new Altas();
                            altas.relacionClientes(idCliente, idCotizacion);
                        }
                    }
                } else {
                    for (int i=0; i<clientes.size(); i++) {
                        for (int j=0; j<cotizaciones.size(); j++) {
                            String queryCliente = "SELECT id FROM Clientes WHERE("+
                                    " Nombres = '"+clientes.get(i).getNombres()+"'"+
                                    " AND Apellidos = '"+clientes.get(i).getApellidos()+"'"+
                                    " AND Direccion = '"+clientes.get(i).getDireccion()+"'"+
                                    " AND Correo = '"+clientes.get(i).getCorreo()+"'"+
                                    " AND Telefono = '"+clientes.get(i).getTelefono()+"'"+
                                    " AND RFC = '"+clientes.get(i).getRFC()+"'"+
                                    " AND NumeroCB = '"+clientes.get(i).getNumeroCB()+"');";
                            Statement statement1 = con.createStatement();
                            ResultSet resultSet1 = statement1.executeQuery(queryCliente);
                            if (resultSet1.next()==false){}
                            else {
                                resultSet1.first();
                                idCliente = resultSet1.getInt("id");
                            }
                            String queryCotizacion = "SELECT id From Cotizaciones WHERE("+
                                    " Modelo = '"+cotizaciones.get(j).getModelo()+"'"+
                                    " AND Cantidad = "+cotizaciones.get(j).getCantidad()+
                                    " AND Precio = "+cotizaciones.get(j).getPrecio()+");";
                            Statement statement2 = con.createStatement();
                            ResultSet resultSet2 = statement2.executeQuery(queryCotizacion);
                            if (resultSet2.next()==false){}
                            else{
                                resultSet2.first();
                                idCotizacion = resultSet2.getInt("id");
                            }
                            Altas altas = new Altas();
                            altas.relacionClientes(idCliente, idCotizacion);
                        }
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error de Conexxion con la base de datos contacte a soporte!");
            }
    }
 
    public ArrayList<Cotizacion> buscarCotizacionesPorCliente(Persona cliente){
        ArrayList<Cotizacion> cotizacionesOut = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String query = "SELECT Cotizaciones.* FROM ClientesCotizaciones"+
                    " INNER JOIN Clientes ON ClientesCotizaciones.idCliente = Clientes.id"+
                    " INNER JOIN Cotizaciones ON ClientesCotizaciones.idCotizacion = Cotizaciones.id"+
                    " WHERE Clientes.Nombres LIKE '%"+cliente.getNombres()+"%'"+
                    " AND Clientes.Apellidos LIKE '%"+cliente.getApellidos()+"%'"+
                    " AND Clientes.RFC LIKE '%"+cliente.getRFC()+"%';";
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            con.close();
            while (resultSet.next()==true){
                cotizacionesOut.add(new Cotizacion(
                        resultSet.getString("Modelo"), 
                        resultSet.getInt("Cantidad"), 
                        resultSet.getDouble("Precio")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error de conexion en la base de datos");
        }
        return cotizacionesOut;
    }
    public ArrayList<Persona> buscarPersonasPorCotizacion(Cotizacion cotizacion){
        ArrayList<Persona> personas = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            String query = "SELECT Clientes.* FROM ClientesCotizaciones"+
                    " INNER JOIN Cotizaciones ON ClientesCotizaciones.idCotizacion = Cotizaciones.id"+
                    " INNER JOIN Clientes ON ClientesCotizaciones.idCliente = Clientes.id"+
                    " WHERE Cotizaciones.Modelo LIKE '%"+cotizacion.getModelo()+"%';";
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            con.close();
            while (resultSet.next()==true) {                
                personas.add(new Persona(
                        resultSet.getString("Nombres"), 
                        resultSet.getString("Apellidos"), 
                        resultSet.getString("Direccion"), 
                        resultSet.getString("Correo"), 
                        resultSet.getString("Telefono"), 
                        resultSet.getString("RFC"), 
                        resultSet.getString("NumeroCB")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Ventas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return personas;
    }

    public void eliminaRelaciones(ArrayList<Persona> personas, ArrayList<Cotizacion> cotizaciones){
        int idCliente=0,idCotizacion=0;
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            for (int i=0; i<personas.size(); i++){
            String queryCliente = "SELECT id FROM Clientes"+
                    " WHERE Nombres = '"+personas.get(i).getNombres()+"'"+
                    " AND Apellidos = '"+personas.get(i).getApellidos()+"'"+
                    " AND RFC = '"+personas.get(i).getRFC()+"';";
                    Statement statement1 = con.createStatement();
                    ResultSet resultSet1 = statement1.executeQuery(queryCliente);
                    if (resultSet1.next()==false){}
                        else {
                            resultSet1.first();
                            idCliente = resultSet1.getInt("id");
                            String queryDelete1 = "DELETE FROM ClientesCotizaciones WHERE idCliente = "+idCliente+" LIMIT 1;";
                            statement1.executeQuery(queryDelete1);
                        }
            }
            for (int i=0; i<cotizaciones.size(); i++) {
            String queryCotizacion = "SELECT id From Cotizaciones WHERE("+
                                " Modelo = '"+cotizaciones.get(i).getModelo()+"'"+
                                " AND Cantidad = "+cotizaciones.get(i).getCantidad()+
                                " AND Precio = "+cotizaciones.get(i).getPrecio()+");";
                        Statement statement2 = con.createStatement();
                        ResultSet resultSet2 = statement2.executeQuery(queryCotizacion);
                        if (resultSet2.next()==false){}
                        else{
                            resultSet2.first();
                            idCotizacion = resultSet2.getInt("id");
                            String queryDelete2 = "DELETE FROM ClientesCotizaciones WHERE idCotizacion = "+idCotizacion+" LIMIT 1;";
                            statement2.executeQuery(queryDelete2);
                        }
            }
            JOptionPane.showMessageDialog(null, "Relaciones eliminadas!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
