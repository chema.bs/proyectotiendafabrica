/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flujos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetos.Material;
import objetos.Producto;

/**
 *
 * @author xam-lx
 */
public class Produccion {
    String[] conexion = {"192.168.124.230","TiendaFinal","xam-db","itvirtual"};
    public void producir(ArrayList<Material> materiales, ArrayList<Producto> productos, int cantidad){
        int flag=0;
        int idMaterial=0,idProducto=0;
        try {
            Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);
            ArrayList<Integer> cantidadMateriales = new ArrayList<>();
            ArrayList<Integer> cantidadProductos = new ArrayList<>();
            for (int i=0; i<materiales.size(); i++) {
                String queryMaterial = "SELECT * FROM Materiales WHERE("+
                        " Nombre = '"+materiales.get(i).getNombre()+"'"+
                        " AND Cantidad = "+materiales.get(i).getCantidad()+
                        " AND Costo = "+materiales.get(i).getCosto()+");";
                            Statement statement1 = con.createStatement();
                            ResultSet resultSet1 = statement1.executeQuery(queryMaterial);
                            if (resultSet1.next()==false){}
                            else {
                                resultSet1.first();
                                cantidadMateriales.add(resultSet1.getInt("Cantidad"));
                            }
            }
            for (int i=0; i<cantidadMateriales.size(); i++) {
                if (cantidadMateriales.get(i)<cantidad){
                    flag=1;
                    JOptionPane.showMessageDialog(null, "No tienes material sufuciente para producir los productos!");
                    break;
                }
            }
            if (flag!=1){
                for (int i=0; i<materiales.size(); i++) {
                    String queryUpdateMaterial = "UPDATE Materiales SET"+
                            " Cantidad = ?"+
                            " WHERE(Nombre = '"+materiales.get(i).getNombre()+"'"+
                            " AND Cantidad = "+materiales.get(i).getCantidad()+
                            " AND Costo = "+materiales.get(i).getCosto()+");";
                    System.out.println(cantidadMateriales.get(i)-cantidad);
                    System.out.println(materiales.get(i).getNombre());
                    System.out.println(materiales.get(i).getCantidad());
                    System.out.println(materiales.get(i).getCosto());
                    PreparedStatement preparedStatement1 = con.prepareStatement(queryUpdateMaterial);
                    preparedStatement1.setInt(1, cantidadMateriales.get(i)-cantidad);
                    preparedStatement1.execute();
                }
                for (int i=0; i<productos.size(); i++) {
                    String queryCantidadMaterial = "SELECT * FROM Productos"+
                            " WHERE (Modelo = '"+productos.get(i).getModelo()+"'"+
                            " AND Cantidad = "+productos.get(i).getCantidad()+
                            " AND Precio = "+productos.get(i).getPrecio()+");";
                    Statement statementPS = con.createStatement();
                    ResultSet resultSetPS = statementPS.executeQuery(queryCantidadMaterial);
                    if (resultSetPS.next()==false){}
                            else {
                                resultSetPS.first();
                                cantidadProductos.add(resultSetPS.getInt("Cantidad"));
                    }
                }
                for (int i=0; i<productos.size(); i++) {
                    String queryUpdateProducto = "UPDATE Productos SET"+
                            " Cantidad = ?"+
                            " WHERE (Modelo = '"+productos.get(i).getModelo()+"'"+
                            " AND Precio = "+productos.get(i).getPrecio()+
                            " AND Cantidad = "+productos.get(i).getCantidad()+");";      
                    PreparedStatement preparedStatement2 = con.prepareStatement(queryUpdateProducto);
                    preparedStatement2.setInt(1, cantidadProductos.get(i)+cantidad);
                    preparedStatement2.execute();
                }
                con.close();
                JOptionPane.showMessageDialog(null, "Actualizacoon correcta!");
            }
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion en la base de datos!");
            ex.printStackTrace();
        }
    }
    
}
