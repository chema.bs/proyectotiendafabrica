
package visual;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import objetos.Usuario;

public class Login extends javax.swing.JFrame {
  String[] conexion = {"127.0.0.1","tienda","root","mn77tFyrfpMJVrqHoY"};
    public Login() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Entrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Usuario");

        jLabel2.setText("Contraseña");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPasswordField1)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(46, 46, 46))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public ArrayList<Usuario> accesoValidar(Usuario usuario, Usuario password){
        ArrayList<Usuario> outPersonas = new ArrayList<>();
        try (Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);){
            
            String query = "SELECT user_type, username, password FROM Usuarios"+
                            "WHERE username = '"+usuario.getUsername()+"'"+
                            "AND password ='"+usuario.getPassword()+"'";
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            con.close();
            while(resultSet.next()==true){
                        System.out.print(resultSet.getString("username"));
                        System.out.print(resultSet.getString("password"));
            }
        } catch (SQLException ex) {
            //ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error de conexion con base de datos!");
        }

        return outPersonas;
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String user = jTextField1.getText();
        String pass = String.valueOf(jPasswordField1.getPassword());
        
        accesoValidar(user, pass);
        /*/
        if("adm".equals(jTextField1.getText()) && "adm".equals( String.valueOf(jPasswordField1.getPassword()) )){
            //JOptionPane.showMessageDialog(null, jTextField1.getText());
              Administrador admin = new Administrador();
              admin.setVisible(true);
        }else if("empleado".equals(jTextField1.getText()) && "empleado".equals( String.valueOf(jPasswordField1.getPassword()) )){
            JOptionPane.showMessageDialog(null, jTextField1.getText());
            Empleado empleado = new Empleado();
            empleado.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, "Denegado!");
        }/*/
        
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        
        java.awt.EventQueue.invokeLater(() -> {
            Login login = new Login();
            login.setLocation(300, 300);
            login.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables

    private void accesoValidar(String user, String pass) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try (Connection con = DriverManager.getConnection("jdbc:mariadb://"+
                    conexion[0]+":3306/"+conexion[1],conexion[2],conexion[3]);){
            
            String query = "SELECT user_type, username, password FROM Usuarios"+
                            "WHERE username = '"+usuario.getUsername()+"'"+
                            "AND password ='"+usuario.getPassword()+"'";
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            con.close();
            while(resultSet.next()==true){
                        System.out.print(resultSet.getString("username"));
                        System.out.print(resultSet.getString("password"));
            }
        } catch (SQLException ex) {
            //ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error de conexion con base de datos!");
        }
    }
}
